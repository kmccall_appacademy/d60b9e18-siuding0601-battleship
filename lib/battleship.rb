require "./lib/board"
require "./lib/player"
require 'byebug'


class BattleshipGame
  attr_accessor :board, :player, :display

  def initialize(player, board)
    @player = player
    # @comp = ComputerPlayer.new
    @board = board
  end

  def attack(position)

    if board[position] == :s
      p "You hit a ship!"
      board.display[position[0]][position[1]] = :s
    else
      p "That was just the open waters..."
      board.in_range?(position)
      board.display[position[0]][position[1]] = :x
    end
    board[position] = :x
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    p "There are #{board.count} ships left!"
    size = board.display.count
      size.times.each do |i|
        p board.display[i]
      end
    p "Where do you want to attack (Row, Column)?"
    input = gets.chomp
    position = player.get_play(input)
    self.attack(position)
  end

  def play
    board.populate_grid
    while board.won? == false
      self.play_turn
    end
    puts "You won!"
  end





end

# game = BattleshipGame.new(HumanPlayer.new, Board.new)
# game.play

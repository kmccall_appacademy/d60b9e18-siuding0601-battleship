require 'byebug'
class Board

attr_accessor :grid, :display

  def initialize(grid = Board.default_grid)
    @grid = grid
    size = grid[0].count
    @display = Array.new(size) {Array.new(size, nil)}
  end

  def self.default_grid
    Array.new(10) {Array.new(10, nil)}
  end

  def count
    @grid.flatten.count(:s)
  end

  def in_range?(position)
    row = position[0]
    killed = self.display[row].count(:s)
    remaining = @grid[row].count(:s)
    if remaining >= killed
      p "You are close ;)"
    else
      p "Try another row!"
    end
  end

  def populate_grid
    size = (@grid[0].count ** 2 ) / 20
    until self.count == size
      self.place_random_ship
    end
  end

  def empty?(position = nil)
    if position != nil && @grid[position[0]][position[-1]] == nil
      return true
    elsif position == nil
      if self.count == 0
        return true
      else
        return false
      end
    end
  end

  def full?
    if self.grid.flatten.count(nil) == 0
      return true
    else
      return false
    end
  end

  def place_random_ship
    length = @grid[0].count
    if self.full?
      raise "Board is full"
    else
      @grid[rand(0..length-1)][rand(0..length-1)] = :s
    end
  end

  def won?
    if self.grid.flatten.count(:s) == 0
      return true
    else
      return false
    end
  end

  # NEW
  def [](key)
    @grid[key[0]][key[1]]
  end

  def []=(key,value)
    @grid[key[0]][key[1]]=value
  end

end

class HumanPlayer
  def get_play(input)
    input.delete(", ").split("").map(&:to_i)
  end
end
